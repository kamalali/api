<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\PostResource;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class PostsController extends Controller
{

    use ApiResponseTrait;
    public function index()
    {
        $posts = PostResource::collection(Post::paginate($this->paginate));
        return $this->apiResponse($posts);
    }


    public function show($id)
    {

        $post = Post::find($id);
        if ($post) {
            return $this->apiResponse(new PostResource($post));
        }
        return $this->noResponse();

    }


    public function store(Request $request)
    {
        $validation = $this->validation($request);
        if ($validation instanceof Response) {
            return $validation;
        }
        $post = Post::create($request->all());
        if ($post) {
            return $this->createdResponse(new PostResource($post));
        }
        return $this->apiResponse(null, 'Can Not Add New', 404);
    }


    public function update($id, Request $request)
    {
        $validation = $this->validation($request);

        if ($validation instanceof Response) {
            return $validation;
        }
        $post = Post::find($id);
        if (!$post) {
            return $this->noResponse();
        }
        $post->update($request->all());
        if ($post) {
            return $this->apiResponse(new PostResource($post), null, 201);
        }
        return $this->apiResponse(null, 'Un Know Error', 520);

    }

    public function delete($id)
    {
        $post = Post::find($id);
            if($post)
            {
                $post->delete();
                return $this->deletedResponse();
            }
            return $this->noResponse();

    }

    public function validation($request)
    {
        return $this->apiValidate($request, [
            'title' => 'required',
            'body' => 'required',
        ]);
    }

}

